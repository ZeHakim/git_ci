const test = require('tape'); // assign the tape library to the variable "test"
var somme = require("./somme")
test('sum should return the addition of two numbers', function (t) {
    t.equal(3, somme.sum(1, 2)); // make this test pass by completing the add function!
    t.equal(4, somme.sum(2, 2));
    t.equal(290, somme.sum(345, -55));
    t.equal(1, somme.sum(-1, 2));
    t.notEqual(3, somme.sum(3, 67));
    t.equal(0, somme.sum(55, -55));
    t.equal(Int, somme.sum(2, 2));
    t.end();
  });